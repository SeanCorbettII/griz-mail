<?php
  session_start();
  require('oauth.php');
  require('outlook.php');

  $loggedIn = !is_null($_SESSION['access_token']);
  $redirectUri = 'http://localhost/server/authorize.php';
?>
<html>
    <head>
      <title>PHP Mail API Tutorial</title>
    </head>
  <body>
    <?php
      if (!$loggedIn) {
    ?>
      <!-- User not logged in, prompt for login -->
      <p>Please <a href="<?php echo oAuthService::getLoginUrl($redirectUri)?>">sign in</a> with your Office 365 or Outlook.com account.</p>
    <?php
      }
      else {
        $messages = OutlookService::getMessages($_SESSION['access_token'], $_SESSION['user_email']);
    ?>
      <!-- User is logged in, do something here -->
      <p>Messages: <?php echo print_r($messages) ?></p>
    <?php
      }
    ?>
  </body>
</html>
